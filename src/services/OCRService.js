import {createWorker, PSM} from 'tesseract.js';

const OCRService = (function () {

    const LANGUAGE_ID_GERMAN = "deu";
    const LANGUAGE_ID_ENGLISH = "eng";
    const BASE64_IDENTIFIER = ";base64,";
    const FULL_LANGUAGE_STRING = LANGUAGE_ID_GERMAN;
    //+ "+" + LANGUAGE_ID_ENGLISH;
    let worker = null;


    async function init() {
        const isInit = true;
        if (worker) {
            await worker.terminate();
        }
        worker = await initalizeWorker(FULL_LANGUAGE_STRING);
        return isInit;
    }

    async function cleanup() {
        const isClean = true;
        await worker.terminate();
        return isClean;
    }


    async function initalizeWorker(language) {
        const worker = createWorker({
            logger: m => console.log(m)
        });
        await worker.load();
        await worker.loadLanguage(language);
        await worker.initialize(language);
        //siehe https://yvonnickfrin.dev/ocr-in-javascript-with-tesseract
        // und https://github.com/tesseract-ocr/tesseract/blob/4.0.0/src/ccstruct/publictypes.h#L163
        //AUTO
        await worker.setParameters({
            tessedit_pageseg_mode: PSM.AUTO,
        });
        return worker;
    }


    //Code for Libary-Use ist mostly copied from example
    // see following URL for explenation
    // https://github.com/naptha/tesseract.js#tesseractjs
    // https://github.com/naptha/tesseract.js/blob/master/docs/examples.md
    // https://github.com/naptha/tesseract.js/blob/master/docs/api.md
    async function recognizeText(image) {
        // recycling workers for faster results
        if (!worker) {
            worker = await initalizeWorker(FULL_LANGUAGE_STRING);
        }
        const result = await worker.recognize(image);
        const {data: {text}} = result;
        return text;
    }


    return {
        recognizeText: recognizeText,
        cleanup: cleanup,
        init: init
    }
})();


export default OCRService;
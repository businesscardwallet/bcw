# Business Card Wallet
This Project is also a businesscard-Wallet and a Businesscard-Reader developed as PWA. It f

The app is fully offline-capable. The recognition of the corresponding contact data is carried out by a Javascript OCR library on the respective mobile end device. 

In addition, the app makes it possible to import and export vCards via QR code.
## Workbox and Service-Worker
For adding static-Files and Librarys to precache, use the workbox-cli

Currently .qz, .js Files and the Icons (.png,.svg,.ico) from Public-Folder will be added and precached.

Because of the big Training-Data-Files, be carefull with your mobile-Data.

Install workbox-cli

``npm install workbox-cli --global``

Then run 

``workbox injectManifest``

And remove comment in src/service-worker.js in line 27 so it looks like this:

``const babelFiles = self.__WB_MANIFEST;``
## Test me!
You can test me without building

``set HTTPS=true&&npm start``

Open then the browser:

```https://ip-adress-of-your-computer:3000/```

## Build me!
When it is the first time, then get all the dependencies:

``npm install``

For Building the Project

``npm run build``
## Serve me!
When you don't want to rebuild the App, you can start it from the build Directory.

Install serve oder http-server by:

``
npm install --global serve
``

And then

``
serve -s build
``

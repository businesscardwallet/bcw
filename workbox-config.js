//File was generated with workbox-cli and then customized
//see: https://developers.google.com/web/tools/workbox/modules/workbox-cli
//see: https://developers.google.com/web/tools/workbox/guides/generate-service-worker/cli
module.exports = {
  "globDirectory": "public/",
  "globPatterns": [
    "**/*{eng.traineddata.gz,deu.traineddata.gz,.js,.png,.svg,.ico}"
  ],
  "swDest": "src/service-worker.js",
  "maximumFileSizeToCacheInBytes": "50000000",
  "swSrc": "src/service-worker_template.js"
};